from typing import Union, Tuple
from mysql.connector import connect , cursor, Error as mysqlerror
from mysql.connector.cursor import MySQLCursorNamedTuple
from logging import basicConfig, getLogger

basicConfig(
    format='%(asctime)s %(levelname)s %(name)s %(message)s',
    filename="/your/absolute/path/to/file.log",
    level="INFO"
)
custom_logger = getLogger(__name__)
database = {
    'user': "username",
    'password': "password",
    'host': "localhost",
    'database' : "databasename",
    'raise_on_warnings': True,
    'use_pure' : False
}

def _select(
        query: str,
        valuemapper: object=None,
        onerow: bool=False,
        db: str=database,
        bufferedQuery: bool=False
    ) -> Union[None, Tuple[object], object]:
    try:
        cnx = connect(**db)
        cursor = cnx.cursor(
            buffered=bufferedQuery,
            named_tuple=True
        )
        cursor.execute(
            query,
            valuemapper
        )
        if onerow:
            result = cursor.fetchone()
        else:
            result = cursor.fetchall()
    except mysqlerror as e:
        custom_logger.error(
            "SQL {} {} {}".format(
                e,
                query,
                valuemapper
            )
        )
        result = None
    finally:
        if cnx.is_connected():
            cursor.close()
            cnx.close()
    return result

def _insert_update(
        query: str,
        valuemapper: Union[dict, tuple],
        db: str=database
    ) -> bool:
    try:
        cnx = connect(**db)
        cnx.autocommit = False
        cursor = cnx.cursor()
        cursor.execute(
            query,
            valuemapper
        )
        result = True
    except mysqlerror as e:
        cnx.rollback()
        result = False
        custom_logger.error(
            "SQL {} {} {}".format(
                e,
                query,
                valuemapper
            )
        )
    finally:
        cnx.commit()
        if cnx.is_connected():
            cursor.close()
            cnx.close()
    return result